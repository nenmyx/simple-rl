import math
from typing import NewType

from tcod import tcod

import config
import constants.intent
from constants.trap_types import TrapType

Glyph = NewType("Glyph", str)
Name = NewType("Name", str)
RGB = NewType("RGB", tuple[int, int, int])


class GraphicsData:
    symbol: Glyph
    rgb: RGB

    def __init__(self, symbol: Glyph, rgb: RGB):
        self.symbol = symbol
        self.rgb = rgb


class Position:
    """X and Y coordinate container."""

    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

    def distance_to(self, other: "Position") -> int:
        term1 = (self.x - other.x) ** 2 + (self.y - other.y) ** 2
        return math.ceil(math.sqrt(term1))


class Velocity(Position):
    """Velocity coordinate."""
    pass


class Input:
    def __init__(self) -> None:
        self.key_move_down = False
        self.key_move_up = False
        self.key_move_left = False
        self.key_move_right = False
        self.key_use = False
        self.key_run = False
        self.key_rebind = False
        self.key_door = False
        self.key_get = False
        self.key_fire = False
        self.key_music = False

        # Default keys.

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def reset(self) -> None:
        for prop in vars(self):
            if prop.startswith("__"):
                pass

            if not prop.startswith("key_"):
                pass

            setattr(self, prop, False)

    def any(self) -> bool:
        for prop in vars(self):
            if prop.startswith("__"):
                pass

            if not prop.startswith("key_"):
                pass

            res = getattr(self, prop, False)
            if not res:
                continue
            else:
                return res
        return False

    def any_movement(self) -> bool:
        for prop in vars(self):
            if prop.startswith("__"):
                pass

            if not prop.startswith("key_move"):
                pass

            res = getattr(self, prop, False)
            if not res:
                continue
            else:
                return res
        return False


class Viscera:
    """The things within a body."""

    def __init__(self, hp: int, atk: int, xp: int, level: int, food: int = 100) -> None:
        self.hp = hp
        self.max_hp = hp
        self.atk = atk
        self.xp = xp
        self.level = level
        self.food = food


class MobInternals:
    def __init__(self, intent: str):
        self.intent = getattr(constants.intent, intent)
        self.calcify_cooldown = config.calcify_cooldown
        self.patrol_points = None
        self.target_point = None


class PlayerInternals:
    def __init__(self):
        self.aiming = False
        self.aim_tile = [-1, -1]
        self.damage_timer = 0

        self.keycodes = {
            'key_move_down': tcod.event.KeySym.DOWN, 'key_move_up': tcod.event.KeySym.UP,
            'key_move_left': tcod.event.KeySym.LEFT, 'key_move_right': tcod.event.KeySym.RIGHT,
            'key_use': tcod.event.KeySym.SPACE, 'key_run': tcod.event.KeySym.r,
            'key_rebind': tcod.event.KeySym.F1, 'key_get': tcod.event.KeySym.g,
            'key_fire': tcod.event.KeySym.f, 'key_music': tcod.event.KeySym.m}


class Sword:
    def __init__(self, damage: int) -> None:
        self.damage = damage


class Bow:
    def __init__(self, range_tiles: int, damage: str, ammo: str):
        self.range_tiles = range_tiles
        self.damage = damage
        self.ammo = ammo


class Trap:
    def __init__(self,  trap_type: TrapType = TrapType.Hunger):
        self.trap_type = trap_type