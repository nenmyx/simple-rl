import sys
from pathlib import Path

import soundfile
import tcod
from tcod.sdl.audio import Channel, BasicMixer


class AudioController:
    def __init__(self):
        self.mixer = BasicMixer(tcod.sdl.audio.open())
        self.muted = False
        try:
            asset_path = Path(sys._MEIPASS, 'res')
        except Exception:
            asset_path = Path('res')

        bgm_file = asset_path / 'Skulk (Sketch 2).ogg'

        self.sound, self.sample_rate = soundfile.read(file=bgm_file)

        self.sound = self.mixer.device.convert(sound=self.sound, rate=self.sample_rate)
        self.bgm_channel: Channel = None
        self.bgm_start()

    def bgm_end(self, channel: Channel) -> None:
        if not self.muted:
            self.bgm_start()

    def bgm_start(self) -> None:
        self.bgm_channel = self.mixer.play(sound=self.sound, on_end=self.bgm_end)

    def bgm_stop(self, time: int) -> None:
        self.bgm_channel.fadeout(time=time)

    def bgm_toggle(self, fadeout: int = 1) -> None:
        if self.bgm_channel.busy:
            self.muted = True
            self.bgm_stop(time=fadeout)
        else:
            self.muted = False
            self.bgm_start()
