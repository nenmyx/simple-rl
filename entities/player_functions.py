from random import randint
from typing import Tuple, Optional, List

import tcod.ecs
import xdice
from tcod.ecs.entity import Entity
from tcod.ecs.registry import Registry

import config
import render_functions
from constants import tags, tile_types
from components import *
from dungeon import trap_effects
from dungeon.game_map import GameMap


def init_player(registry: Registry) -> Entity:
    """
    Initialize player entity.

    :param registry: The registry to create the entity in.
    :type registry: tcod.ecs.Registry
    :return: The initialized player entity.
    :rtype: Entity
    """
    entity = registry.new_entity()
    entity.components[Name] = Name(config.player_name)
    entity.components[GraphicsData] = GraphicsData(symbol=Glyph("@"), rgb=constants.color.black)
    entity.components[Position] = Position(x=-1, y=-1)
    entity.components[Input] = Input()
    entity.components[Viscera] = Viscera(hp=config.player_base_hp, atk=config.player_base_atk, xp=0, level=1)
    entity.components[Sword] = Sword(damage=0)
    entity.components[PlayerInternals] = PlayerInternals()
    entity.components[Bow] = Bow(range_tiles=10, ammo=15, damage='1d4')
    entity.tags.add(tags.IsPlayer)
    entity.tags.add(tags.IsSolid)
    return entity


def fight(player: Entity, other: Entity) -> [str]:
    """
    :param player: The player entity.
    :type player: Entity
    :param other: The defending entity.
    :type other: Entity
    :rtype: [str]

    This method performs a fight between the player and the other entity. It reduces the
    other entity's HP based on the player's attack attribute. It also updates the player's
    XP if the other entity's HP becomes zero or less. Additionally, it generates loot on
    the other entity's death if necessary.
    """
    result = []
    player_viscera = player.components[Viscera]
    player_weapon = player.components[Sword]
    other_viscera = other.components[Viscera]

    damage_roll = max(1, player_viscera.atk + randint(1, 4))
    damage_roll += player_weapon.damage
    other_viscera.hp -= damage_roll
    result.append('Dealt %s damage to %s.' % (damage_roll, other.components[Name]))
    result = resolve_combat(player=player, other=other, output=result)

    return result


def ranged_attack(player: Entity, mob: Entity) -> Optional[List[str]]:
    """
    Use the player's bow to attack the specified mob.
    :param player: The player entity.
    :type player: Entity
    :param mob: The attack target.
    :type mob: Entity
    :rtype: Optional[List[str]]
    """
    if tags.IsMob not in mob.tags:
        # Why.
        return None

    output = []
    player_bow = player.components[Bow]
    mob_viscera = mob.components[Viscera]

    # Position check for integrity.
    player_position = player.components[Position]
    mob_position = mob.components[Position]
    distance = player_position.distance_to(mob_position)
    if distance >= player_bow.range_tiles:
        player_bow.ammo -= 1
        output.append('You missed.')
        return output

    if player_bow.ammo <= 0:
        output.append('You lack arrows.')
        return output

    damage_roll = xdice.roll(player_bow.damage)
    mob_viscera.hp -= damage_roll
    player_bow.ammo -= 1
    output.append('Dealt %s damage to %s.' % (damage_roll, mob.components[Name]))
    output = resolve_combat(player=player, other=mob, output=output)

    return output


def resolve_combat(player: Entity, other: Entity, output: [str]) -> List[str]:
    other_viscera = other.components[Viscera]
    player_viscera = player.components[Viscera]
    if other_viscera.hp <= 0:
        player_viscera.xp += other_viscera.xp
        output.append('%s died. You gain %s XP.' % (other.components[Name], other_viscera.xp))
        other.clear()

        if player_viscera.xp >= 100:
            output.append(level_up(player))

    return output


def level_up(player: Entity) -> str:
    """
    :param player: The player entity.
    :type player: Entity

    Reset player's XP to zero. Roll attack and HP increases.
    """
    viscera = player.components[Viscera]
    atk_roll = xdice.roll('1d8')
    hp_roll = xdice.roll('2d10')
    viscera.atk += atk_roll
    viscera.hp += hp_roll
    viscera.max_hp += hp_roll
    viscera.xp = 0
    viscera.level += 1
    return 'Welcome to level %s. You gained %s attack.' % (viscera.level, atk_roll)


def move(player: Entity, direction: Tuple[int, int], registry: Registry, game_map: GameMap) -> ([str], bool):
    """
    :param player: The player entity.
    :type player: Entity
    :param direction: The direction to move.
    :type direction: Tuple[int, int]
    :param registry: The registry to create the entity in.
    :type registry: Registry
    :param game_map: The game map.
    :type game_map: GameMap
    :rtype: Tuple[[str], bool]
    :return: Output to be appended to the game log.
    """
    result = []
    player_position = player.components[Position]

    # Boundary checking.
    target = (player_position.x + direction[0], player_position.y + direction[1])
    x_bound = target[0] <= 0 or target[0] >= game_map.width
    y_bound = target[1] <= 0 or target[1] >= game_map.height
    if x_bound or y_bound:
        return result, False

    # Entity interaction processing.
    if game_map.entities[target[0], target[1]]:
        entity_hash = game_map.entities[target[0], target[1]]
        entity = registry[entity_hash]
        is_solid = tags.IsSolid in entity.tags
        is_mob = tags.IsMob in entity.tags
        is_exit = tags.IsExit in entity.tags
        is_food = tags.IsFood in entity.tags

        if is_solid and is_mob:
            if Viscera in entity.components:
                # Fight the mob and append the output to the ledger.
                result += fight(player, entity)
            return result, False
        elif is_solid:
            result.append('Your movement is blocked by a %s' % entity.components[Name])
            return result, False
        elif is_exit:
            return result, True
        elif is_food:
            food_roll = randint(1, 10) + 5
            player.components[Viscera].food += food_roll
            entity.clear()
            game_map.entities[target[0], target[1]] = None
            result.append('You gained %s rations.' % food_roll)
        else:
            result.append('You move onto a %s.' % (entity.components[Name]))

    if game_map.tiles[target[0], target[1]] == tile_types.wall:
        result.append('There is a wall there.')
        return result, False

    # Trap interaction processing.
    if game_map.traps[target[0], target[1]]:
        trap_entity = game_map.traps[target[0], target[1]]
        trap_component = trap_entity.components[Trap]
        trap_output = trap_effects.find(player=player, trap_type=trap_component.trap_type, game_map=game_map)
        trap_entity.clear()
        game_map.traps[target[0], target[1]] = None
        if trap_output:
            for line in trap_output:
                result.append(line)

    player_position.x = target[0]
    player_position.y = target[1]

    viscera = player.components[Viscera]
    internals = player.components[PlayerInternals]
    if viscera.food <= 0:
        starvation_damage = randint(1, 10) + 3
        viscera.hp -= starvation_damage
    else:
        internals.damage_timer += 1
        if randint(1, 3) == 1:
            viscera.food -= 1
        if viscera.hp < viscera.max_hp:
            if internals.damage_timer >= 4:
                if randint(1, 3) == 1:
                    hp_roll = randint(1, 2)
                    viscera.hp = min(viscera.hp + hp_roll, viscera.max_hp)

    render_functions.update_fov(player, game_map)
    return result, False


def get(player: Entity, game_map: GameMap, registry: Registry) -> Optional[List[str]]:
    result = []
    position_component = player.components[Position]

    entity_guid = game_map.entities[position_component.x, position_component.y]
    if entity_guid is None:
        return None

    entity = registry[entity_guid]
    if Sword not in entity.components:
        return None

    if Sword in player.components:
        result.append('You discard your +%s weapon.' % player.components[Sword].damage)

    player.components[Sword] = entity.components[Sword]
    result.append('You equip a %s.' % entity.components[Name])
    entity.clear()
    return result
