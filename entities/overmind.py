import random
from typing import Optional, List, Tuple

import numpy as np
import xdice
from numpy import ndarray
from tcod import path
from tcod.ecs import Registry, Entity

import config
from components import MobInternals, Position, Viscera, Name, PlayerInternals
from constants import tags, tile_types, intent
from dungeon.game_map import GameMap
from dungeon.trap_generator import TrapGenerator


def build_pathfinding_map(game_map: GameMap) -> ndarray:
    """
    Use dungeon tiles and solid entity positions to create a pathfinding map.

    :param game_map: GameMap
    :type game_map: GameMap
    :return: Generated pathfinding map
    :rtype: ndarray
    """
    pathfinding_map = np.full(shape=(game_map.width, game_map.height), fill_value=1, order="F")
    for x in range(0, game_map.width - 1):
        for y in range(0, game_map.height - 1):
            if game_map.tiles[x, y] == tile_types.wall:
                pathfinding_map[x, y] = 0
    return pathfinding_map


def fight_player(mob: Entity, player: Entity) -> str:
    """
    Apply mob's atk to player's HP.

    :param mob: Attacking mob
    :type mob: Entity
    :param player: Defending player
    :type player: Entity
    :return: Ledger output
    :rtype: str
    """
    mob_viscera, player_viscera = mob.components[Viscera], player.components[Viscera]
    player_internals = player.components[PlayerInternals]
    player_internals.damage_timer = 0
    player_viscera.hp -= mob_viscera.atk

    if player_viscera.hp <= 0:
        return '%s dealt %s damage, SLAYING YOU!' % (mob.components[Name], mob_viscera.atk)
    else:
        return '%s dealt %s damage to you.' % (mob.components[Name], mob_viscera.atk)


class Overmind:
    """
    Acts as the central hub for non-player behavior.
    """
    def __init__(self, registry: Registry, game_map: GameMap, player: Entity):
        self.registry = registry
        self.game_map = game_map
        self.player = player
        self.messages = []
        self.trap_generator = TrapGenerator(registry=self.registry)

        pathfinding_map = build_pathfinding_map(game_map)
        self.graph = path.SimpleGraph(cost=pathfinding_map, cardinal=1, diagonal=0)

    def act(self) -> [str]:
        """
        Process actions for every mob.

        :return: List of status messages.
        :rtype: list[str]
        """
        self.messages = []
        for mob in self.registry.Q.all_of(tags=[tags.IsMob]):
            internals = mob.components[MobInternals]

            result: Optional[List[str]] = None
            match internals.intent:
                case intent.calcifier:
                    self.act_calcify(mob=mob, game_map=self.game_map)

                case intent.aggressive:
                    result = self.act_aggressive(mob=mob)

            if result is not None:
                self.messages.append(result)

        return self.messages

    def act_calcify(self, mob: Entity, game_map: GameMap) -> None:
        mob_position = mob.components[Position]
        mob_internals = mob.components[MobInternals]

        if mob_internals.calcify_cooldown <= 0:
            mob_internals.calcify_cooldown = config.calcify_cooldown
            if xdice.roll('1d4') != 1:
                return

            if game_map.traps[mob_position.x, mob_position.y]:
                return
            trap = self.trap_generator.generate()
            trap.components[Position] = Position(mob_position.x, mob_position.y)
            # Add the new trap to the dungeon.
            self.game_map.traps[mob_position.x, mob_position.y] = trap
        # Calcifier ability not on cooldown.
        else:
            if not mob_internals.patrol_points:
                # Initialize patrol points.
                mob_internals.patrol_points = []
                for _ in range(1, 5):
                    mob_internals.patrol_points.append(game_map.get_random_position())
                mob_internals.target_point = random.choice(mob_internals.patrol_points)

            # Move to our target point so that we can spread traps and destruction.
            pathfinder = path.Pathfinder(graph=self.graph)
            pathfinder.add_root((mob_position.x, mob_position.y))
            pf = pathfinder.path_to(mob_internals.target_point).tolist()[1:]
            if len(pf) > 0:
                target = (pf[0][0], pf[0][1])
                self.try_move(mob=mob, target=target)
            mob_internals.calcify_cooldown -= 1

            if mob_position.x == mob_internals.target_point[0] and mob_position.y == mob_internals.target_point[1]:
                mob_internals.target_point = random.choice(mob_internals.patrol_points)


    def act_aggressive(self, mob: Entity) -> Optional[str]:
        """
        If the player is within range, move towards them. If within melee range, attack.

        :param mob: Attacking mob
        :type mob: Entity
        :return: Ledger output
        :rtype: Optional[str]
        """
        pathfinder = path.Pathfinder(graph=self.graph)
        player_position = self.player.components[Position]
        mob_position = mob.components[Position]
        distance_to_player = player_position.distance_to(other=mob_position)
        player_out_of_range = distance_to_player > config.mob_alert_radius
        player_can_see_us = self.game_map.visible[mob_position.x, mob_position.y]

        if player_out_of_range or not player_can_see_us:
            return None

        pathfinder.add_root((mob_position.x, mob_position.y))
        pf = pathfinder.path_to((player_position.x, player_position.y)).tolist()[1:]


        if len(pf) < 1:
            target_x, target_y = mob.components[Position].x, mob.components[Position].y
        else:
            target_x, target_y = pf[0][0], pf[0][1]

        if (target_x, target_y) == (player_position.x, player_position.y):
            return fight_player(mob=mob, player=self.player)

        self.try_move(mob=mob, target=(target_x, target_y))
        return None

    def try_move(self, mob: Entity, target: Tuple[int, int]):
        target_x, target_y = target
        prev_x, prev_y = mob.components[Position].x, mob.components[Position].y

        if self.game_map.entities[target_x, target_y]:
            return

        if self.game_map.tiles[target_x, target_y] == tile_types.wall:
            return

        self.game_map.entities[prev_x, prev_y] = None
        self.game_map.entities[target_x, target_y] = mob.uid

        mob.components[Position].x = target_x
        mob.components[Position].y = target_y
