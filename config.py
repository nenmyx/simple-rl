game_window_width, game_window_height = 80, 60
ui_offset: int = 5
disable_intro: bool = False

# Player Config
player_base_hp: int = 40
player_base_atk: int = 1
player_name: str = 'Mlem'
player_sight_radius: int = 15

# Mob Config
mob_alert_radius: int = 15
calcify_cooldown:int = 10

# Dungeon Config
use_fog_of_war: bool = True
easy_exit: bool = False
starting_depth: int = 1
final_depth: int = 5

# Render Config
text_frame_margin: int = 8
text_frame_title: str = 'Skulk'
text_frame_decoration: str = '╔═╗║ ║╚═╝'
