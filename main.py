#!/usr/bin/env python3
import sys
from pathlib import Path

import tcod.console
import tcod.context
import tcod.event
import tcod.tileset
import tcod.ecs

from config import game_window_width, game_window_height
from engine import Engine
import logging.config

try:
    logging.config.fileConfig(Path(sys._MEIPASS) / "logging.conf")
except Exception:
    logging.config.fileConfig('logging.conf')
logger = logging.getLogger('simple-rl')


def main() -> None:
    """Main game loop."""
    logger.info('Loading asset.')
    try:
        tileset_path = Path(sys._MEIPASS, "res", "dejavu10x10_gs_tc.png")
    except Exception:
        tileset_path = Path("res/dejavu10x10_gs_tc.png")
    tileset = tcod.tileset.load_tilesheet(
        tileset_path, 32, 8, tcod.tileset.CHARMAP_TCOD,
    )

    logger.info('Initializing console.')
    console = tcod.console.Console(game_window_width, game_window_height, order="F")

    logger.info('Initializing game engine.')
    engine = Engine(console=console)

    logger.info('Initializing tcod context.')
    with tcod.context.new(
            columns=console.width, rows=console.height, tileset=tileset, title='Skulk'
    ) as context:
        while True:
            engine.update(context)
            context.present(console)


if __name__ == "__main__":
    main()
