from tcod import tcod
from tcod.event import Event, KeySym

from components import Input, PlayerInternals


def handle_event(event: Event, input_component: Input, player_internals: PlayerInternals) -> Input:
    """
    Flip associated bits on the given input object in response to TCOD event scan.
    # todo: Load user-configurable keybinds to support non-US (and non-QWERTY) keyboards.

    :param event: TCOD event object
    :type event: Event
    :param input_component: Keyboard input data object.
    :type input_component: Input
    :return: Modified keyboard input data object.
    :rtype: Input
    """
    match event:
        case tcod.event.Quit():
            raise SystemExit()
        case tcod.event.KeyUp(sym=tcod.event.KeySym.ESCAPE):
            raise SystemExit()

    if event.type == 'KEYUP':
        for item in player_internals.keycodes.items():
            if KeySym(item[1]) == KeySym(event.sym):
                setattr(input_component, item[0], True)

    return input_component
