# simple-rl

A barebones traditional roguelike created for [The Post-Weird Dream Jam](https://itch.io/jam/the-post-weird-dream-jam).

## Vibes
* Cynicism
* Post-truth
* Post-weird

## To-Do
* Configuration
* UI
* Take Ms Menardi's advice and build some custom systems for fun.
* Colored text?

## Has a working implementation
* Dungeon generation
* Combat

## Done

## Canceled
* Door (verb) (also noun)
  * Canceled due to ??? how make good doors ???