from enum import Enum

class TrapType(Enum):
    Disarm = 1
    Damage = 2
    Hunger = 3
    Teleport = 4
    Ambush =  5