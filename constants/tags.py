from typing import Final

IsPlayer: Final = "IsPlayer"
IsMob: Final = "IsMob"
IsWall: Final = "IsWall"
IsSolid: Final = "IsSolid"
IsExit: Final = "IsExit"
IsFood: Final = "IsFood"
IsWeapon: Final = "IsWeapon"
