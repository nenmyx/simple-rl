from typing import List, Optional


class Ledger:
    messages: [str]
    size: int

    def __init__(self, size: int, depth: int):
        self.messages = []
        self.size = size

        match depth:
            case 1:
                self.messages.append('You choke down your hesitation.')
                self.messages.append('We\'re through the threshold now.')
                self.messages.append('<Press F1 to configure keys.>')

    def add_message(self, message: str):
        if len(self.messages) == self.size:
            self.messages.pop()
        self.messages.append(message)

    def add_messages(self, messages: Optional[List[str]]):
        for message in messages:
            if message == "" or message is None:
                continue
            self.add_message(message)
