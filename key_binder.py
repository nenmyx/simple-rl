
from tcod import tcod
from tcod.console import Console
from tcod.context import Context
from tcod.event import KeySym

from components import Input, PlayerInternals
from render_functions import Drawing


class KeyBinder:
    def __init__(self, console: Console, drawing: Drawing):
        self.console = console
        self.drawing = drawing
        self.inputs = (
            ('key_move_down', 'Down'),
            ('key_move_up', 'Up'),
            ('key_move_left', 'Left'),
            ('key_move_right', 'Right'),
            ('key_use', 'Menu/Wait'),
            ('key_run', 'Run'),
            ('key_rebind', 'Rebind'),
            ('key_get', 'Get'),
            ('key_fire', 'Fire'),
            ('key_music', 'Music toggle.'),
        )

        self.counter = 0

    def act(self, player_internals: PlayerInternals, context: Context) -> bool:
        if self.counter > len(self.inputs):
            return False

        input = self.inputs[self.counter]

        for event in tcod.event.wait():
            processed_event = context.convert_event(event=event)
            if processed_event.type == 'KEYUP':
                player_internals.keycodes[input[0]] = KeySym(processed_event.sym)
                self.counter += 1
                if self.counter < len(self.inputs):
                    return True
                else:
                    return False

    def prompt(self) -> [str]:
        input = self.inputs[self.counter]
        lines = ['Press the key you wish to assign to...', input[1]]
        return lines
