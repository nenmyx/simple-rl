import logging

import tcod.console
from tcod.context import Context
from tcod.ecs.entity import Entity

import config
import event_handling
import render_functions
from audio import AudioController
from components import Input, Viscera, PlayerInternals, Position
from constants import directions, tags
from dungeon import procgen
from dungeon.game_map import GameMap
from dungeon.mob_generator import MobGenerator
from entities import player_functions
from entities.overmind import Overmind
from key_binder import KeyBinder
from ledger import Ledger

system_logger = logging.getLogger('skulk.engine')


# noinspection PyTypeChecker
def input_to_direction(input_component: Input) -> (int, int):
    direction = (0, 0)
    if input_component.key_move_up:
        direction = directions.up
    elif input_component.key_move_down:
        direction = directions.down
    elif input_component.key_move_left:
        direction = directions.left
    elif input_component.key_move_right:
        direction = directions.right
    return direction


class Engine:
    # Keep this file small.
    game_map: GameMap = None
    player: Entity = None

    def __init__(self, console: tcod.console.Console) -> None:
        """
        Initializes an instance of the class.

        :param console: A tcod.console.Console object representing the game console.
        :type console: tcod.console.Console
        :return: None
        """
        self.logger = logging.getLogger('skulk.engine.Engine')
        self.registry = tcod.ecs.Registry()
        self.player = player_functions.init_player(registry=self.registry)
        self.console = console
        self.init_map()
        render_functions.update_fov(self.player, self.game_map)
        self.ledger = Ledger(size=50, depth=self.game_map.depth)
        self.overmind = Overmind(registry=self.registry, player=self.player, game_map=self.game_map)
        self.intro_playing = not config.disable_intro
        self.game_over = False
        self.escaped = False
        self.rebinding = False
        self.drawing = render_functions.Drawing(self.console, self.registry)
        self.key_binder = KeyBinder(console=self.console, drawing=self.drawing)
        self.mob_generator = MobGenerator(registry=self.registry)
        self.audio_controller = AudioController()

    def reset_crawl(self) -> None:
        self.console.clear()
        self.registry = tcod.ecs.Registry()
        self.player = player_functions.init_player(registry=self.registry)
        self.init_map()
        render_functions.update_fov(self.player, self.game_map)
        self.ledger = Ledger(size=50)
        self.overmind = Overmind(registry=self.registry, player=self.player, game_map=self.game_map)
        self.intro_playing = not config.disable_intro
        self.game_over = False
        self.escaped = False
        self.drawing = render_functions.Drawing(self.console, self.registry)
        self.mob_generator = MobGenerator(registry=self.registry)

    def init_map(self) -> None:
        """
        Initialize the game map and keep a reference to it.
        :return: None"""
        entity = self.registry.new_entity()
        self.game_map = procgen.generate_floor(
            registry=self.registry,
            width=self.console.width,
            height=self.console.height - config.ui_offset)
        entity.components[GameMap] = self.game_map

    def descend(self) -> None:
        """
        Replace our map with one of depth + 1.
        """
        self.purge_entities()
        depth = self.game_map.depth
        if depth == config.final_depth:
            self.logger.info(msg='Final floor completed!')
            self.game_over = True
            self.escaped = True
            return
        elif depth > config.final_depth:
            self.logger.error(msg='Attempted to generate floor with a depth greater than the final depth.')
            raise SystemExit

        # Now that we're done checking for the end of the game, create a new floor.
        self.game_map = procgen.generate_floor(
            registry=self.registry,
            width=self.console.width,
            height=self.console.height - config.ui_offset,
            depth=depth + 1)
        render_functions.update_fov(self.player, self.game_map)

    def update(self, context: Context) -> None:
        """

        Update the game state.

        This method handles user input, updates entities in the game map,
        clearing the console, rendering entities and HUD elements.

        :param context: The game context.
        :type context: Context
        """
        self.console.clear()
        input_component = Input()
        for event in tcod.event.wait():
            processed_event = context.convert_event(event=event)
            event_handling.handle_event(
                event=processed_event,
                input_component=input_component,
                player_internals=self.player.components[PlayerInternals])

        if input_component.key_rebind:
            self.rebinding = True
            self.key_binder = KeyBinder(console=self.console, drawing=self.drawing)
            self.ledger.add_messages(self.key_binder.prompt())
            self.render()
            return

        if self.rebinding:
            internals = self.player.components[PlayerInternals]
            acting = self.key_binder.act(player_internals=internals, context=context)
            if acting:
                self.ledger.add_messages(self.key_binder.prompt())
            else:
                self.rebinding = False
                self.ledger.add_message(message='Finished rebinding keys.')
            self.render()
            return


        if input_component.key_music:
            self.audio_controller.bgm_toggle()

        # Once the game is over, draw the appropriate ending screen.
        if self.game_over:
            # What we're saying here is "we've won the game but not escaped" i.e. big dead.
            key_pressed = False
            if not self.escaped:
                key_pressed = self.drawing.play_defeat(input_component=input_component)
            else:
                key_pressed = self.drawing.play_victory(input_component=input_component)

            if key_pressed:
                self.reset_crawl()
                self.drawing.play_intro(input_component=input_component)
            return

        # We continually redraw the intro to wait for input.
        if self.intro_playing:
            self.intro_playing = not self.drawing.play_intro(input_component=input_component)
            if not self.intro_playing:
                render_functions.update_fov(self.player, self.game_map)
                self.render()
            return

        if self.intro_playing:
            return

        player_internals = self.player.components[PlayerInternals]

        if player_internals.aiming:
            if input_component.key_fire:
                uid = self.game_map.entities[player_internals.aim_tile[0], player_internals.aim_tile[1]]
                mob = self.registry[uid]
                player_internals.aiming = False
                result = player_functions.ranged_attack(player=self.player, mob=mob)
                if result is not None:
                    self.ledger.add_messages(messages=result)

            direction = input_to_direction(input_component=input_component)
            if direction != (0, 0):
                player_internals.aim_tile[0] += direction[0]
                player_internals.aim_tile[1] += direction[1]
            self.render()
            return

        self.game_map.update_entities(registry=self.registry)
        render_functions.update_fov(self.player, self.game_map)
        result = self.handle_input(input_component=input_component)
        player_acted, descended = result[0], result[1]
        if descended:
            self.descend()
            if self.game_over:
                self.drawing.play_victory(input_component=input_component)
                return
            self.overmind = Overmind(registry=self.registry, player=self.player, game_map=self.game_map)
            self.render()
            return

        if player_acted:
            overmind_output = self.overmind.act()
            self.ledger.add_messages(messages=overmind_output)
        if self.player.components[Viscera].hp <= 0:
            self.drawing.play_defeat(input_component=input_component)
            self.game_over = True
        else:
            self.render()


    def render(self) -> None:
        """
        Call rendering helpers.
        """
        self.game_map.render(console=self.console)
        self.drawing.draw_entities(game_map=self.game_map)
        self.drawing.draw_status_bar(player=self.player)
        self.drawing.draw_ledger(ledger=self.ledger)

        if self.player.components[PlayerInternals].aiming:
            self.drawing.draw_crosshair(coordinates=self.player.components[PlayerInternals].aim_tile)

    def handle_input(self, input_component: Input) -> (bool, bool):
        """

        Handles the input from the input_component.

        :param input_component: The input component.
        :type input_component: Input

        :return: Whether the player took an action or not, and whether we descended floors.
        :rtype: Tuple[bool, bool]

        """
        if not input_component.any():
            return False, False

        if not input_component.any_movement():
            return False, False

        direction = input_to_direction(input_component)

        if direction != (0, 0):
            output = player_functions.move(
                player=self.player, direction=direction, registry=self.registry, game_map=self.game_map
            )

            self.ledger.add_messages(output[0])
            return True, output[1]

        if input_component.key_fire:
            player_internals = self.player.components[PlayerInternals]
            player_position = self.player.components[Position]
            player_internals.aiming = True
            player_internals.aim_tile = [player_position.x, player_position.y]
            return True, False

        if input_component.key_get:
            # Pick up a solid from the ground.
            get_result = player_functions.get(player=self.player, game_map=self.game_map, registry=self.registry)
            if get_result is not None:
                self.ledger.add_messages(get_result)
            return True, False

        # Did not take action.
        return False, False

    def entity_map(self, m):
        return m is not None and (tags.IsMob not in self.registry[m].tags)

    def purge_entities(self) -> None:
        """
        Clear all entities besides the player.
        """
        for entity in self.registry.Q.all_of(tags=[tags.IsMob]):
            entity.clear()

        for entity in self.registry.Q.all_of(tags=[tags.IsExit]):
            entity.clear()
