from typing import Optional, List

import xdice
from tcod.ecs import Entity

from components import Viscera, Position
from constants.trap_types import TrapType
from dungeon.game_map import GameMap


def find(player: Entity, trap_type: TrapType, game_map: GameMap) -> Optional[List[str]]:
    match trap_type:
        case trap_type.Hunger:
            return hunger(player)
        case trap_type.Teleport:
            return teleport(player=player, game_map=game_map)
        case _:
            return None


def teleport(player: Entity, game_map: GameMap) -> Optional[List[str]]:
    new_position = game_map.get_random_position()
    player_position = player.components[Position]

    player_position.x = new_position[0]
    player_position.y = new_position[1]

def hunger(player: Entity) -> Optional[List[str]]:
    """
    Executes when the player moves onto a Hunger trap.

    :param player: Entity containing player data.
    :type player: Entity
    :return: Entity containing player data.
    """
    value = xdice.roll('1d8')
    viscera = player.components[Viscera]
    viscera.food -= value

    return ['Stepped on a Hunger Trap. %s food deducted.' % value]
