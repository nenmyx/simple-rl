import csv
import sys
from pathlib import Path
from typing import Optional, List

import xdice
from tcod.ecs import Registry, Entity

from components import Name, GraphicsData, Glyph, RGB, Position, Viscera, MobInternals
from constants import color, tags


class Record:
    def __init__(self, row, header):
        self.__dict__ = dict(zip(header, row))


def load_record(filename: str):
    result = {}
    try:
        assets_path = Path(sys._MEIPASS, 'res')
    except Exception:
        assets_path = Path('res')
    file_path = assets_path / filename
    with open(file_path, 'r') as f:
        data = list(csv.reader(f, delimiter=','))
        header = data.pop(0)
        for row in data:
            result[row[0]] = Record(row, header)
    return result


class MobGenerator:
    def __init__(self, registry: Registry):
        self.registry = registry
        self.prototypes = load_record('mobs.csv')
        self.floors = load_record('floors.csv')

    def summon_prototype(self, name: str, quantity: int) -> Optional[List[Entity]]:
        if name not in self.prototypes.keys():
            return None

        result = []
        for i in range(quantity):
            prototype = self.prototypes[name]
            mob = self.registry.new_entity()
            mob.tags.add(tags.IsMob)
            mob.tags.add(tags.IsSolid)
            mob.components[Position] = Position(x=-1, y=-1)
            mob.components[Name] = Name(prototype.name)
            mob.components[GraphicsData] = GraphicsData(symbol=Glyph(prototype.glyph),
                                                        rgb=RGB(getattr(color, prototype.color)))
            hp_roll = xdice.roll(prototype.hproll)
            xp_roll = xdice.roll(prototype.xproll)
            atk_roll = xdice.roll(prototype.atkroll)
            mob.components[Viscera] = Viscera(hp=hp_roll, atk=atk_roll, xp=xp_roll, level=1)
            mob.components[MobInternals] = MobInternals(intent=prototype.intent)
            result.append(mob)

        return result
