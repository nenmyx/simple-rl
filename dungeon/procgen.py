import random
from typing import Tuple, Iterator

import numpy as np
import tcod.ecs.registry
import xdice
from tcod import tcod
from tcod.ecs.entity import Entity
from tcod.ecs.registry import Registry

import config
from components import Name, Position, Glyph, GraphicsData, RGB, Sword
from constants import color, tags, tile_types
from dungeon.game_map import GameMap, Room
from dungeon.mob_generator import MobGenerator


def tunnel_between(
        start: Tuple[int, int], end: Tuple[int, int]
) -> Iterator[Tuple[int, int]]:
    """
    Generate coordinates for a tunnel between two points.

    :param start: The starting point of the tunnel as a tuple (x, y).
    :param end: The ending point of the tunnel as a tuple (x, y).
    :return: An iterator that yields the coordinates of the tunnel.

    Example usage:
    start = (2, 2)
    end = (8, 8)
    rng = tcod.random.Random()
    for x, y in tunnel_between(start, end, rng):
        print(f"({x}, {y})")
    """
    x1, y1 = start
    x2, y2 = end
    if random.randint(0, 1) == 0:
        # Move horizontally, then vertically.
        corner_x, corner_y = x2, y1
    else:
        # Move vertically, then horizontally.
        corner_x, corner_y = x1, y2

    # Generate the coordinates for this tunnel.
    for x, y in tcod.los.bresenham((x1, y1), (corner_x, corner_y)).tolist():
        yield x, y
    for x, y in tcod.los.bresenham((corner_x, corner_y), (x2, y2)).tolist():
        yield x, y


def generate_floor(registry: Registry, width: int, height: int, depth: int = config.starting_depth) -> GameMap:
    """
    Generates a floor for the game.

    Parameters:
    - registry: A Registry object that stores game entities and components.
    - width: An integer representing the width of the floor map.
    - height: An integer representing the height of the floor map.

    Returns:
    - A GameMap object representing the generated floor map.

    """
    dungeon = GameMap(width, height, depth=depth)
    nodes = {}
    rooms = []
    mobgen = MobGenerator(registry=registry)

    tree = tcod.bsp.BSP(x=0, y=0, width=width, height=height)
    tree.split_recursive(
        depth=depth+6,
        min_width=6,
        min_height=6,
        max_horizontal_ratio=1.5,
        max_vertical_ratio=1.5,
    )

    for node in tree.pre_order():
        if node.children:
            margin = random.randint(0, 3)
            node_1, node_2 = node.children
            room_1 = Room(node_1.x + margin, node_1.y + margin, node_1.width - margin, node_1.height - margin)
            room_2 = Room(node_2.x + margin, node_2.y + margin, node_2.width - margin, node_2.height - margin)
            nodes[node_1.__hash__()] = room_1
            nodes[node_2.__hash__()] = room_2
            for x, y in tunnel_between(room_1.center(), room_2.center()):
                dungeon.tiles[x, y] = tile_types.floor
        else:
            if node.__hash__() in nodes.keys():
                room = nodes[node.__hash__()]
                dungeon.tiles[room.inner] = tile_types.floor
                rooms.append(room)

    dungeon.rooms = rooms
    place_entities(dungeon=dungeon, registry=registry, mob_gen=mobgen)
    return dungeon


def place_entities(dungeon: GameMap, registry: Registry, mob_gen: MobGenerator):
    floor_data = mob_gen.floors[str(dungeon.depth)]
    num_mobs = xdice.roll(floor_data.mob_roll)
    num_food = xdice.roll(floor_data.ration_roll)
    num_weapons = xdice.roll(floor_data.weapon_roll)
    entities = []
    dungeon.entities = np.full((dungeon.width, dungeon.height), fill_value=None, order="F")

    mob_pool = [floor_data.mob1, floor_data.mob2, floor_data.mob3, floor_data.mob4, floor_data.mob5]

    for _ in range(num_mobs):
        mob = mob_gen.summon_prototype(name=random.choice(mob_pool), quantity=1)
        entities.append(mob[0])

    for _ in range(num_food):
        food = create_food(registry=registry)
        entities.append(food)

    for _ in range(num_weapons):
        weapon = create_weapon(registry=registry, pattern=floor_data.weapon_value)
        entities.append(weapon)

    for entity in registry.Q.all_of(tags=[tags.IsPlayer]):
        entities.append(entity)
        player = entity
        break

    for _ in range(xdice.roll('1d4')):
        entities.append(create_exit(registry=registry))

    for mob in entities:
        place_entity(mob, dungeon)

    if config.easy_exit:
        for entity in registry.Q.all_of(tags=[tags.IsExit]):
            stairs = entity
            break
        exit_position = stairs.components[Position]
        place_next_to(subject=player, target=exit_position, dungeon=dungeon)


def create_weapon(registry: Registry, pattern: str) -> Entity:
    entity = registry.new_entity()

    dmg_roll = xdice.roll(pattern_string=pattern)
    entity.components[Sword] = Sword(damage=dmg_roll)
    entity.components[Name] = Name('Sword +%s' % dmg_roll)
    entity.components[Position] = Position(x=-1, y=-1)
    entity.components[GraphicsData] = GraphicsData(symbol=Glyph("?"), rgb=RGB(color.sword))
    entity.tags.add(tag=tags.IsWeapon)
    return entity


def create_food(registry: Registry) -> Entity:
    entity = registry.new_entity()
    entity.tags.add(tags.IsFood)
    entity.components[Position] = Position(-1, -1)
    entity.components[GraphicsData] = GraphicsData(symbol=Glyph('+'), rgb=RGB(color.black))
    entity.components[Name] = Name("Food")
    return entity


def create_exit(registry: Registry) -> Entity:
    entity = registry.new_entity()
    entity.tags.add(tags.IsExit)
    entity.components[Position] = Position(-1, -1)
    entity.components[GraphicsData] = GraphicsData(symbol=Glyph('>'), rgb=RGB(color.white))
    entity.components[Name] = Name('Exit')
    return entity


def place_next_to(subject: Entity, target: Position, dungeon: GameMap) -> None:
    position = subject.components[Position]
    x = target.x + random.randint(-1, 1)
    y = target.y + random.randint(-1, 1)
    while dungeon.entities[x, y]:
        x = target.x + random.randint(-1, 1)
        y = target.y + random.randint(-1, 1)
    position.x = x
    position.y = y


def place_entity(mob: Entity, dungeon: GameMap) -> None:
    """
    Places an entity in a random location within the dungeon.

    Parameters:
    mob (Entity): The entity to be placed in the dungeon.
    dungeon (GameMap): The game map representing the dungeon.

    Returns:
    None
    """
    x, y = dungeon.get_random_position()

    position = mob.components[Position]
    position.x = x
    position.y = y
    dungeon.entities[x, y] = mob.uid
