import random

from tcod.ecs import Registry, Entity

from components import Position, Trap, Name
from constants import trap_types


class TrapGenerator:
    """
    Provides a simple interface to instantiate a trap.
    """
    def __init__(self, registry: Registry):
        self.registry = registry
        self.trap_types = [trap_types.TrapType.Hunger, trap_types.TrapType.Teleport]

    def generate(self) -> Entity:
        """
        Pull a new Trap from the pool.

        :return: Object representing the trap.
        :rtype: Entity
        """
        entity = self.registry.new_entity()
        entity.components[Position] = Position(-1, -1)
        entity.components[Trap] = Trap(trap_type=random.choice(self.trap_types))
        entity.components[Name] = Name('Trap')
        return entity