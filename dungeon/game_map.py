import random
from typing import Tuple

import numpy as np
from tcod.console import Console
from tcod.ecs import Registry, Entity

import config
from constants import tile_types, tags
from components import Position


class Room:
    """
    A class used to represent a game room.
    """
    x1: int
    y1: int
    x2: int
    y2: int

    def __init__(self, x: int, y: int, width: int, height: int) -> None:
        self.x1 = x
        self.y1 = y
        self.x2 = self.x1 + width
        self.y2 = self.y1 + height

    def center(self) -> Tuple[int, int]:
        """
        Calculate the center of the room.

        :rtype: (int, int)
        :return: the center of the room.
        """
        x = (self.x2 + self.x1) // 2
        y = (self.y2 + self.y1) // 2
        return x, y

    @property
    def inner(self) -> Tuple[slice, slice]:
        """
        Calculate the inner bounds of the room.

        :rtype: Tuple[slice, slice]
        :return: the inner bounds of the room.
        """
        return slice(self.x1 + 1, self.x2 - 1), slice(self.y1 + 1, self.y2 - 1)

    @property
    def width(self) -> int:
        """
        Calculate the width of the room.

        :rtype: int
        :return: the width of the room.
        """
        return self.x2 - self.x1

    @property
    def height(self) -> int:
        """
        Calculate the height of the room.

        :rtype: int
        :return: the height of the room.
        """
        return self.y2 - self.y1


class GameMap:
    def __init__(self, width: int, height: int, depth: int = config.starting_depth):
        self.width, self.height = width, height
        self.tiles = np.full((width, height), fill_value=tile_types.wall, order="F")

        self.visible = np.full((width, height), fill_value=False, order="F")
        self.explored = np.full((width, height), fill_value=False, order="F")

        self.entities = np.full((width, height), fill_value=None, order="F")
        self.traps = np.full((width, height), fill_value=None, order="F")
        self.rooms: [Room] = []
        self.depth = depth

    def in_bounds(self, x: int, y: int) -> bool:
        """
        Return True if x and y are inside the bounds of this map.

        :param x: X coordinate
        :type x: int
        :param y: Y coordinate
        :type y: int
        :rtype: bool
        """
        return 0 <= x < self.width and 0 <= y < self.height

    def render(self, console: Console) -> None:
        """
        Updates the console RGB values to render the game map.

        :param console: The game console to render on.
        :type console: Console
        :return: None
        """
        console.rgb[0:self.width, 0:self.height] = np.select(
            condlist=[self.visible, self.explored],
            choicelist=[self.tiles["light"], self.tiles["dark"]],
            # Conditional here allows user to see the dungeon layout before exploring it.
            default=tile_types.shroud if config.use_fog_of_war else self.tiles["dark"]
        )

    def update_entities(self, registry: Registry) -> None:
        """
        Update the collection of entities within the game map.

        This function updates the internal record of entities within the game map by iterating through
        all entities in the provided registry that have a `Position` component, storing these entities
        internally for further processes.

        :param registry: The registry from which entities are to be collected.
        :type registry: Registry
        :return: None
        """
        self.entities = np.full((self.width, self.height), fill_value=None, order="F")
        for entity in registry.Q.all_of(components=[Position]).none_of(tags=[tags.IsPlayer]):
            self._update_entity_map(entity=entity)

    def get_random_position(self) -> Tuple[int, int]:
        room = random.choice(self.rooms)
        x = random.randint(room.inner[0].start, room.inner[0].stop - 1)
        y = random.randint(room.inner[1].start, room.inner[1].stop - 1)
        return x, y

    def _update_entity_map(self, entity: Entity) -> None:
        """
        Update the entity map with the given entity.

        :param: entities: the entities to update
        :type entity: Entity
        :rtype: None
        """
        position = entity.components[Position]
        if self.in_bounds(position.x, position.y):
            self.entities[position.x, position.y] = entity.uid
