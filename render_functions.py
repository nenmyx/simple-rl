import logging
import sys
from pathlib import Path
from typing import List

import tcod.ecs
from tcod.ecs import Registry, Entity
from tcod.map import compute_fov

import config
from components import Position, Viscera, GraphicsData, Input, Sword, Bow
from constants import color
from dungeon.game_map import GameMap
from ledger import Ledger


def update_fov(player: Entity, game_map: GameMap) -> None:
    """
    Updates the field of view (FOV) for the player on the game map.

    :param player: The player entity.
    :type player: Entity
    :param game_map: The game map.
    :type game_map: GameMap
    :return: None
    """
    position = player.components[Position]
    game_map.visible[:] = compute_fov(
        transparency=game_map.tiles["transparent"],
        pov=(position.x, position.y),
        radius=config.player_sight_radius
    )
    game_map.explored |= game_map.visible


class Drawing:
    def __init__(self, console: tcod.console.Console, registry: Registry):
        self.render_logger = logging.getLogger(name=__name__)
        self.console = console
        self.registry = registry
        self.crawl_opening: [str] = []
        self.crawl_victory: [str] = []
        self.crawl_defeat: [str] = []
        self.load_screens()

    def load_screens(self) -> None:
        try:
            assets_path = Path(sys._MEIPASS, 'res')
        except Exception:
            assets_path = Path('res')
        file_path = assets_path / 'simple-rl screens.txt'
        content = file_path.read_text(encoding='utf-8')
        content = content.replace('\ufeff', '')
        lines = content.splitlines()
        target = None

        for line in lines:
            match line:
                case 'OPENING':
                    target = self.crawl_opening
                    continue
                case 'DEFEAT':
                    target = self.crawl_defeat
                    continue
                case 'VICTORY':
                    target = self.crawl_victory
                    continue

            if line == '':
                continue

            target.append(line)

    def draw_entities(self, game_map: GameMap) -> None:
        """
        Display the glyph for each entity that has one, at its current position.
        :param game_map: game map.
        :type game_map: GameMap
        :rtype: None
        """
        for entity in self.registry.Q.all_of(components=[Position, GraphicsData]):
            pos_x = entity.components[Position].x
            pos_y = entity.components[Position].y
            graphics_data = entity.components[GraphicsData]
            if game_map.visible[pos_x, pos_y]:
                self.console.print(pos_x, pos_y, graphics_data.symbol, fg=graphics_data.rgb)

    def draw_status_bar(self, player: tcod.ecs.Entity) -> None:
        """
        Use player data to draw the left part of the UI (status bar).

        :param player: Player entity.
        :type player: Entity
        :param depth: Dungeon depth.
        :type depth: int
        :rtype: None
        """
        viscera_component = player.components[Viscera]
        weapon_damage = 0
        if Sword in player.components:
            weapon_damage = player.components[Sword].damage

        current_value = viscera_component.hp
        maximum_value = viscera_component.max_hp
        total_width = 20
        hp_bar_width = int(float(current_value) / maximum_value * total_width)
        arrow_count = player.components[Bow].ammo

        self.console.draw_rect(x=0, y=self.console.height - 1, width=total_width, height=1, ch=1, bg=color.bar_empty)

        if hp_bar_width > 0:
            self.console.draw_rect(
                x=0, y=self.console.height - 1, width=hp_bar_width, height=1, ch=1, bg=color.bar_filled
            )

        self.console.print(
            x=1, y=self.console.height - 1, string=f"HP:   {current_value}/{maximum_value}", fg=color.bar_text
        )

        self.console.print(
            x=1, y=self.console.height - 2, string=f"XP:   {viscera_component.xp}", fg=color.bar_text
        )

        self.console.print(
            x=1, y=self.console.height - 3, string=f"FOOD: {viscera_component.food}", fg=color.bar_text
        )

        self.console.print(
            x=1, y=self.console.height - 4, string=f"ATK:  {viscera_component.atk} + {weapon_damage}", fg=color.bar_text
        )

        self.console.print(
            x=1, y=self.console.height - 5, string=f"ARROW:{arrow_count}", fg=color.bar_text
        )

    def draw_crosshair(self, coordinates: (int, int)):
        self.console.print(x=coordinates[0], y=coordinates[1], fg=color.black, string='X')

    def draw_ledger(self, ledger: Ledger) -> None:
        """
        Display the messages within the ledger in LIFO order.

        :param ledger: Ledger entity.
        :type ledger: Ledger
        :rtype: None
        """
        lines_to_display = 5
        length = len(ledger.messages)
        range_start = max(0, length - lines_to_display)
        displayed_messages = ledger.messages[range_start:length]
        displayed_messages.reverse()
        x_position = 20
        y_position = self.console.height - 1
        for message in displayed_messages:
            self.console.print(x_position, y_position, message)
            y_position -= 1

    def text_panel(self, input_component: Input, lines: List[str]) -> bool:
        """
        Display a bordered sequence of center-aligned ASCII-compatible text.

        :param input_component: Container for keyboard input data.
        :type input_component: Input
        :param lines: Container for text lines.
        :type lines: List[str]

        :return: True if no keys pressed, False otherwise.
        :rtype: bool
        """
        self.console.clear()
        # Floor division, so odd game widths will cause left-favoring drift.
        h_center = config.game_window_width // 2
        self.console.print(
            x=h_center,
            y=3,
            string=config.text_frame_title,
            alignment=tcod.tcod.constants.CENTER)
        self.console.draw_frame(
            x=4,
            y=4,
            width=config.game_window_width - config.text_frame_margin,
            height=config.game_window_height - config.text_frame_margin,
            decoration=config.text_frame_decoration)

        y = 6
        for line in lines:
            self.console.print(
                x=h_center,
                y=y,
                string=line,
                alignment=tcod.tcod.constants.CENTER
            )
            y += 2

        return input_component.key_use

    def play_intro(self, input_component: Input) -> bool:
        return self.text_panel(input_component=input_component, lines=self.crawl_opening)

    def play_victory(self, input_component: Input) -> bool:
        return self.text_panel(input_component=input_component, lines=self.crawl_victory)

    def play_defeat(self, input_component: Input) -> bool:
        return self.text_panel(input_component=input_component, lines=self.crawl_defeat)
